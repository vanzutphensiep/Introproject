import mysql.connector
from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import send_from_directory
import datetime
import os
from werkzeug.utils import secure_filename
from passlib.hash import pbkdf2_sha256
app = Flask(__name__)

# MySQL database configuration
config = {
    'user': 'username',
    'password': 'password',
    'host': 'localhost',
    'database': 'name of database',
    'raise_on_warnings': True
}

# Connecting with the database
db = mysql.connector.connect(**config)
# double check if connection is established
print(" * Connected to the database")

# zorgt dat de {{name}} in index standaard "Account" is 
naam = "Account"

#main page
@app.route('/index')
def main_page():
    try:
        naam = name
    except:
        naam = "Account"
    return render_template ('index.html', naam = naam)

#rooms page
@app.route('/rooms')
def rooms():
    return render_template ('rooms.html', naam = naam)

#no favorites page
@app.route('/favorites')
def favorites():
    return render_template ('favorites.html', naam = naam)

#logout function
@app.route ('/logout')
def logout():
    #clear variables when logging out
    global email
    global name
    global lastname
    email = ''
    name = 'Account'
    lastname = ''
    return redirect('/index')

#account page
@app.route('/account')
def account():
    try:
        global name
        global lastname
        # Set variables name and lastname to global
        cursor = db.cursor(buffered=True)
        sql = "SELECT name, lastname FROM users WHERE email = %s"
        values = (email,)
        cursor.execute(sql, values)
        result = cursor.fetchone()
        name = result[0]
        lastname = result[1]
        return render_template('account.html', name=name, lastname=lastname)
    except:
        return render_template('no-account.html', naam = naam)

#no account page
@app.route('/no-account')
def no_account():
    return render_template ('no-account.html', naam = naam)


#messages page
@app.route('/messages')
def messages():
    return render_template ('messages.html', naam = naam)

#account created function
@app.route('/accountcreated', methods=['POST'])
def account_created():
    password = request.form['psw']
    cpsw = request.form ['cpsw'] 
    #if passwords match, add all data to database
    if password == cpsw:
        email = request.form['email']
        name = request.form['name']
        lastname = request.form['lastname']
        birthdate = request.form['bd']
        nationality = request.form['nationality']
       
        #hash the password
        psw = pbkdf2_sha256.hash(password)
        cursor = db.cursor()
        sql = f"INSERT INTO Users (name, lastname, email, birthdate, nationality, psw) VALUES ('{name}', '{lastname}', '{email}', '{birthdate}', '{nationality}', '{psw}');"
        cursor.execute(sql)
        db.commit()
        print(sql) 
        return redirect('/index')
    else: 
        return redirect ('/')
    
#login function 
@app.route('/login', methods = ['POST'])
def login():
    global email
    #check if login credentials match to database
    email =  request.form['email']
    password = request.form['psw']
    cursor = db.cursor(buffered=True)
    sql = "SELECT psw FROM users WHERE email = %s"
    values = (email,)
    cursor.execute(sql, values)
    result = cursor.fetchone()
    if result is not None:
        stored_password = result[0]
        if pbkdf2_sha256.verify(password, stored_password):
            return redirect('/account')
        else:
            return render_template('wrong-password.html')
    else:
        return render_template('no-account.html')

    
@app.route('/details/<int:id>')
def details(id):
    # Retrieve the details for the selected item based on the provided id
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    # Execute the query to fetch the data from the database
    query = (f"SELECT * FROM house WHERE houseid = {id}")
    cursor.execute(query)
    myresult = cursor.fetchone()

    conn.close()
    # Render the details page with the retrieved details
    return render_template('details.html', house_details=myresult, naam = naam)



#uploadroom page
@app.route('/uploadroom', methods =['GET'])
def uploadroom():
    try: 
        if email: 
            return render_template('uploadroom.html')   
        else:
            return redirect('/no-account')
    except:
        return redirect('/no-account')

#display rooms page
@app.route('/result')
def results():
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    # Execute the query to fetch the data from the database
    query = "SELECT * FROM house"
    cursor.execute(query)
    myresult = cursor.fetchall()

    conn.close()

    # Pass the retrieved data to the template
    return render_template('result.html', house=myresult, naam = naam)



def allowed_file(filename):
    # Check if the file extension is allowed

    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

#succesfull upload function
@app.route('/uploadsuccesfull', methods=['POST'])
def insertdata():
    # Retrieve data from the form
    city = request.form['city']
    streetname = request.form['streetname']
    # Retrieve the uploaded image file
    image = request.files['image']
    if image and allowed_file(image.filename):
        filename = secure_filename(image.filename)
        # Save the uploaded image to the server
        image.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    else:
        # Handle invalid file format or no file uploaded
        filename = None
    
    
    # Retrieve other form data
    housenumber = request.form['housenumber']
    oppervlak = request.form['oppervlak']
    price = request.form['price']
    beschrijving = request.form['beschrijving']
    roomdetails = request.form['roomdetails']
    current_date = datetime.date.today()
    formatted_date = current_date.strftime("%Y-%m-%d")
    naam = name
    achternaam = lastname
    # Insert the data from the form into the database 
    cursor = db.cursor()
    sql = f"INSERT INTO House (city, streetname, housenumber, oppervlak, price, beschrijving, roomdetails, image_filename, date, L_name, L_lastname) VALUES ('{city}', '{streetname}', '{housenumber}', '{oppervlak}', '{price}', '{beschrijving}', '{roomdetails}', '{filename}', '{formatted_date}', '{naam}', '{achternaam}');"
    cursor.execute(sql)
    db.commit()
  
    # Retrieve the last inserted houseid
    houseid = cursor.lastrowid  
    mail = email
    # Update the 'users' table to add the houseid for the specific username
    sql = f"UPDATE users SET house = {houseid} WHERE email = '{mail}';"
    cursor.execute(sql)
    db.commit()

    # Send to uploadsucces.html and display the uploaded image
    return render_template('uploadsucces.html', image_filename=filename, naam = naam)

@app.route('/uploads/<filename>')
def uploaded_image(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


#some setup for the application 
if __name__ == "__main__":
    # Set the upload folder and allowed file extensions
    app.config['UPLOAD_FOLDER'] = 'static\\uploads'
    app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg'}
    app.config['TEMPLATES_AUTO_RELOAD']=True
    app.config['DEBUG'] = True
    app.config['SERVER_NAME'] = "127.0.0.1:5000"
    app.run()