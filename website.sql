CREATE table House ( 
houseid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
city VARCHAR,
streetname VARCHAR,
housenumber VARCHAR,
oppervlak FLOAT,
price FLOAT,
beschrijving VARCHAR,
roomdetails VARCHAR,
date DATE
);

CREATE table Users (
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
name VARCHAR,
lastname VARCHAR,
email VARCHAR,
phone INTEGER,
homeowner BOOLEAN NOT NULL,
house integer,
foreign key (house) references "House" (houseid)
	on UPDATE cascade on DELETE cascade
);

